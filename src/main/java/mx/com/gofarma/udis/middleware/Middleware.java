package mx.com.gofarma.udis.middleware;

import io.undertow.server.HttpServerExchange;

public interface Middleware {
    boolean process( HttpServerExchange exchange );
}
