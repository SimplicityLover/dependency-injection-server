package mx.com.gofarma.udis.middleware;

import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;

public class CorsMiddleware implements Middleware {
    private static final HttpString ACCESS_CONTROL_ALLOW_ORIGIN = new HttpString( "Access-Control-Allow-Origin" );
    private static final HttpString ACCESS_CONTROL_ALLOW_METHODS = new HttpString( "Access-Control-Allow-Methods" );
    private static final HttpString ACCESS_CONTROL_ALLOW_CREDENTIALS = new HttpString( "Access-Control-Allow-Credentials" );
    private static final HttpString ACCESS_CONTROL_ALLOW_HEADERS = new HttpString( "Access-Control-Allow-Headers" );
    private static final HttpString ACCESS_CONTROL_EXPOSE_HEADERS = new HttpString( "Access-Control-Expose-Headers" );
    private static final HttpString ACCESS_CONTROL_MAX_AGE = new HttpString( "Access-Control-Max-Age" );

    @Override
    public boolean process( HttpServerExchange exchange ) {
        String origin = exchange.getRequestHeaders().getFirst( Headers.ORIGIN );
        if ( "OPTIONS".equalsIgnoreCase( exchange.getRequestMethod().toString() ) ) {
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_ORIGIN , origin );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_CREDENTIALS , "true" );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_METHODS , "GET, POST, OPTIONS" );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_HEADERS , "Authorization,Access-Control-Allow-Origin,DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,X-Client-Type" );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_MAX_AGE , 1728000 );
            exchange.getResponseHeaders().add( io.undertow.util.Headers.CONTENT_TYPE , "text/plain; charset=utf-8" );
            exchange.getResponseHeaders().add( io.undertow.util.Headers.CONTENT_LENGTH , 0 );
            exchange.setStatusCode( 204 );
            exchange.endExchange();
            return true;
        } else if ( "POST".equalsIgnoreCase( exchange.getRequestMethod().toString() ) ) {
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_ORIGIN , origin );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_CREDENTIALS , "true" );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_METHODS , "GET, POST, OPTIONS" );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_HEADERS , "Authorization,Access-Control-Allow-Origin,DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,X-Client-Type" );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_EXPOSE_HEADERS , "Content-Length,Content-Range" );
            return false;
        } else if ( "GET".equalsIgnoreCase( exchange.getRequestMethod().toString() ) ) {
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_ORIGIN , origin );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_CREDENTIALS , "true" );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_METHODS , "GET, POST, OPTIONS" );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_ALLOW_HEADERS , "Authorization,Access-Control-Allow-Origin,DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range,X-Client-Type" );
            exchange.getResponseHeaders().add( ACCESS_CONTROL_EXPOSE_HEADERS , "Content-Length,Content-Range" );
            return false;
        }
        return false;
    }
}
