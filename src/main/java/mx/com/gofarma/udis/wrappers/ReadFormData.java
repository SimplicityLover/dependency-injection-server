package mx.com.gofarma.udis.wrappers;

import io.undertow.server.handlers.form.FormData;

import java.io.IOException;
import java.util.*;

public final class ReadFormData {
    private final Map< String, List< String > > values = new HashMap<>();
    private final Map< String, List< ReadFile > > files = new HashMap<>();

    public ReadFormData( FormData formData ) throws IOException {
        for ( String key : formData ) {
            Deque< FormData.FormValue > vals = formData.get( key );
            if ( vals.getFirst().isFileItem() ) {
                List< ReadFile > files = new ArrayList<>();
                for ( FormData.FormValue file : vals ) {
                    files.add( new ReadFile( file ) );
                }
                this.files.put( key , Collections.unmodifiableList( files ) );
            } else {
                this.values.put( key , vals.stream().map( FormData.FormValue::getValue ).toList() );
            }
        }
    }

    public Map< String, List< String > > getAllValues() {
        return values;
    }

    public List< String > getValuesOf( String key ) {
        return values.getOrDefault( key , Collections.emptyList() );
    }

    public Optional< String > getFirstValueOf( String key ) {
        List< String > value = values.get( key );
        if ( value.isEmpty() ) return Optional.empty();
        return Optional.ofNullable( value.get( 0 ) );
    }

    public Map< String, List< ReadFile > > getAllFiles() {
        return files;
    }

    public List< ReadFile > getFilesOf( String key ) {
        return files.getOrDefault( key , Collections.emptyList() );
    }
}
