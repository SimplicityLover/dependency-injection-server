package mx.com.gofarma.udis.wrappers;

import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderMap;

public class Headers {
    private HeaderMap headers;

    public Headers( HttpServerExchange httpServerExchange ) {
        this.headers = httpServerExchange.getRequestHeaders();
    }

    public int size() {
        return this.headers.size();
    }

    public boolean isEmpty() {
        return this.headers.size() == 0;
    }

    public boolean containsKey( String key ) {
        return this.headers.contains( key );
    }

    public String get( String key ) {
        return this.headers.get( key ).getFirst();
    }

    public String getOrDefault( String key , String defaultValue ) {
        if ( !this.headers.contains( key ) ) return defaultValue;
        return this.headers.get( key ).getFirst();
    }
}
