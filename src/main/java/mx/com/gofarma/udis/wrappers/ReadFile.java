package mx.com.gofarma.udis.wrappers;

import io.undertow.server.handlers.form.FormData;
import io.undertow.util.HeaderMap;

import java.io.IOException;
import java.nio.file.Files;

public class ReadFile {
    private final String fileName;
    private final HeaderMap headerMap;
    private final byte[] content;

    public ReadFile( FormData.FormValue fi ) throws IOException {
        this.fileName = fi.getFileName();
        this.headerMap = fi.getHeaders();
        this.content = Files.readAllBytes( fi.getFileItem().getFile() );
    }

    public String getFileName() {
        return fileName;
    }

    public HeaderMap getHeaderMap() {
        return headerMap;
    }

    public byte[] getContent() {
        return content;
    }
}
