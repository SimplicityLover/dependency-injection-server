package mx.com.gofarma.udis.wrappers;

import io.undertow.server.BlockingHttpExchange;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.form.FormParserFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Deque;

public class FormDataMap implements AutoCloseable {
    private static final FormParserFactory formParserFactory = FormParserFactory.builder().build();
    private final BlockingHttpExchange blocking;
    private final FormDataParser parser;
    private final io.undertow.server.handlers.form.FormData formData;

    public FormDataMap( HttpServerExchange exchange ) {
        this.blocking = exchange.startBlocking();
        this.parser = formParserFactory.createParser( exchange );
        this.parser.setCharacterEncoding( StandardCharsets.UTF_8.toString() );
        try {
            this.formData = this.parser.parseBlocking();
        } catch ( IOException e ) {
            throw new RuntimeException( "Unable to parse form body" , e );
        }
    }

    public boolean contains( String name ) {
        return this.formData.contains( name );
    }

    public Deque< FormData.FormValue > get( String name ) {
        return this.formData.get( name );
    }

    public FormData.FormValue getFirst( String name ) {
        return this.formData.getFirst( name );
    }

    public FormData.FormValue getLast( String name ) {
        return this.formData.getLast( name );
    }

    @Override
    public void close() throws Exception {
        try {
            if ( this.blocking != null ) this.blocking.close();
        } catch ( Exception ignored ) {
        }
        try {
            if ( this.parser != null ) this.parser.close();
        } catch ( Exception ignored ) {
        }
    }
}
