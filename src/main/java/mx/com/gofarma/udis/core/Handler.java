package mx.com.gofarma.udis.core;

import io.undertow.server.BlockingHttpExchange;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormData;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.form.FormParserFactory;
import io.undertow.util.Headers;
import mx.com.gofarma.udis.annotations.*;
import mx.com.gofarma.udis.exceptions.InvalidMethodException;
import mx.com.gofarma.udis.exceptions.InvalidParameterTypeForFromFormDataAnnotationException;
import mx.com.gofarma.udis.exceptions.MissingParameterException;
import mx.com.gofarma.udis.wrappers.ApiResponse;
import mx.com.gofarma.udis.wrappers.ReadFormData;
import mx.com.gofarma.udis.wrappers.ServerExchange;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Handler implements HttpHandler {
    private static final Object INSTANCE = new Object();
    private static final Logger logger = LoggerFactory.getLogger( Handler.class );
    private static final FormParserFactory.Builder formDataParserBuilder = FormParserFactory.builder();
    private final UndertowDependencyInjectionServer server;
    private final Parameter[] parameters;
    private final ConcurrentHashMap< Parameter, BiFunction< HttpServerExchange, HandlersParameterArray, Object > > handlersParametersMappers = new ConcurrentHashMap<>(); //Parameter , Function(Exchange->Object)
    private final ConcurrentHashMap< Parameter, Pair< BiFunction< HttpServerExchange, Parameter, Object >, Function< Object, Object > > > httpParameterMappers = new ConcurrentHashMap<>(); //Parameter , Function(Exchange,Parameter->Object) , Function(Object->ParameterType)
    private final Method method;
    private final Route route;
    private final String[] allowedHttpMethods;
    private final boolean isRegexHandler;
    private Pattern pattern;

    @SuppressWarnings( "unchecked" )
    public Handler( UndertowDependencyInjectionServer server , Method method , Route route ) {
        this.server = server;
        this.method = method;
        this.route = route;
        this.method.setAccessible( true );
        this.parameters = method.getParameters();
        this.isRegexHandler = !route.regex().isEmpty();
        this.allowedHttpMethods = Arrays.stream( route.methods() ).map( Object::toString ).toArray( String[]::new );
        if ( this.isRegexHandler ) {
            this.pattern = Pattern.compile( route.regex() );
        }
        for ( Parameter parameter : this.parameters ) {
            if ( this.server.getObjectSuppliers().containsKey( parameter.getType() ) ) {
                handlersParametersMappers.put( parameter , this.server.getObjectSuppliers().get( parameter.getType() ) );
            } else {
                BiFunction< HttpServerExchange, Parameter, Object > biFunction = null;
                Function< Object, Object > function = null;

                if ( parameter.isAnnotationPresent( FromJsonBody.class ) ) {
                    biFunction = Handler::getFromBodyToByteArray;
                } else if ( parameter.isAnnotationPresent( FromHeader.class ) ) {
                    biFunction = Handler::getFromHeader;
                } else if ( parameter.isAnnotationPresent( FromFormData.class ) ) {
                    if ( !ReadFormData.class.isAssignableFrom( parameter.getType() ) ) throw new InvalidParameterTypeForFromFormDataAnnotationException();
                    biFunction = Handler::getFromFormDataToFormData;
                } else if ( parameter.isAnnotationPresent( FromPath.class ) ) {
                    biFunction = ( httpServerExchange , p ) -> Handler.getFromPath( httpServerExchange , p , this.pattern );
                } else if ( parameter.isAnnotationPresent( FromQuery.class ) ) {
                    biFunction = Handler::getFromQueryString;
                } else if ( parameter.isAnnotationPresent( FromCookie.class ) ) {
                    biFunction = Handler::getFromCookie;
                } else {
                    continue;
                }

                if ( parameter.isAnnotationPresent( FromJsonBody.class ) ) {
                    function = ( input ) -> ServerExchange.readValue( ( byte[] ) input , parameter.getType() );
                } else if ( String.class.equals( parameter.getType() ) ) {
                    function = Object::toString;
                } else if ( Integer.class.equals( parameter.getType() ) ) {
                    function = Handler::fromObjectToInteger;
                } else if ( Long.class.equals( parameter.getType() ) ) {
                    function = Handler::fromObjectToLong;
                } else if ( Double.class.equals( parameter.getType() ) ) {
                    function = Handler::fromObjectToDouble;
                } else if ( Boolean.class.equals( parameter.getType() ) ) {
                    function = Handler::fromObjectToBoolean;
                } else if ( LocalDateTime.class.equals( parameter.getType() ) ) {
                    function = Handler::fromObjectToLocalDateTime;
                } else if ( Instant.class.equals( parameter.getType() ) ) {
                    function = Handler::fromObjectToInstant;
                } else if ( UUID.class.equals( parameter.getType() ) ) {
                    function = Handler::fromObjectToUUID;
                } else {
                    function = Handler::fromObjectToObject;
                }

                httpParameterMappers.put( parameter , Pair.of( biFunction , function ) );
            }
        }
    }

    private static Object getFromBodyToByteArray( HttpServerExchange exchange , Parameter parameter ) {
        try {
            exchange.startBlocking();
            return IOUtils.toByteArray( exchange.getInputStream() );
        } catch ( IOException e ) {
            return null;
        }
    }

    private static Object getFromFormDataToFormData( HttpServerExchange exchange , Parameter parameter ) {
        try ( BlockingHttpExchange blockingExchange = exchange.startBlocking() ; FormDataParser formDataParser = Handler.formDataParserBuilder.build().createParser( exchange ) ) {
            FormData formData = formDataParser.parseBlocking();
            return new ReadFormData( formData );
        } catch ( IOException e ) {
            return null;
        }
    }

    private static Object getFormDataParser( HttpServerExchange exchange , Parameter parameter ) {
        try ( BlockingHttpExchange blockingExchange = exchange.startBlocking() ) {
            return IOUtils.toString( blockingExchange.getInputStream() , StandardCharsets.UTF_8 );
        } catch ( IOException e ) {
            return null;
        }
    }

    private static Object getFromPath( HttpServerExchange exchange , Parameter parameter , Pattern pattern ) {
        Matcher matcher = pattern.matcher( exchange.getRequestPath() );
        if ( !matcher.matches() ) return null;
        return matcher.group( parameter.getAnnotation( FromPath.class ).value() );
    }

    private static Object getFromHeader( HttpServerExchange exchange , Parameter parameter ) {
        String name = parameter.getAnnotation( FromHeader.class ).value();
        if ( !exchange.getRequestHeaders().contains( name ) ) return null;
        return exchange.getRequestHeaders().get( name ).getFirst();
    }

    private static Object getFromQueryString( HttpServerExchange exchange , Parameter parameter ) {
        String name = parameter.getAnnotation( FromQuery.class ).value();
        if ( !exchange.getQueryParameters().containsKey( name ) ) return null;
        return exchange.getQueryParameters().get( name ).getFirst();
    }

    private static Object getFromCookie( HttpServerExchange exchange , Parameter parameter ) {
        String name = parameter.getAnnotation( FromCookie.class ).value();
        if ( !exchange.getRequestCookies().containsKey( name ) ) return null;
        return exchange.getRequestCookies().get( name ).getValue();
    }

    private static Object fromObjectToObject( Object object ) {
        return object;
    }

    private static Object fromObjectToInteger( Object object ) {
        if ( object == null ) return null;
        return Integer.parseInt( object.toString() );
    }

    private static Object fromObjectToLong( Object object ) {
        if ( object == null ) return null;
        return Long.parseLong( object.toString() );
    }

    private static Object fromObjectToDouble( Object object ) {
        if ( object == null ) return null;
        return Double.parseDouble( object.toString() );
    }

    private static Object fromObjectToBoolean( Object object ) {
        if ( object == null ) return null;
        return Boolean.parseBoolean( object.toString() );
    }

    private static Object fromObjectToLocalDateTime( Object object ) {
        if ( object == null ) return null;
        return LocalDateTime.ofInstant( Instant.parse( object.toString() ) , ZoneId.systemDefault() );
    }

    private static Object fromObjectToInstant( Object object ) {
        if ( object == null ) return null;
        return Instant.parse( object.toString() );
    }

    private static Object fromObjectToUUID( Object object ) {
        if ( object == null ) return null;
        return UUID.fromString( object.toString() );
    }

    private void verifyHttpMethod( HttpServerExchange httpServerExchange ) {
        for ( String method : allowedHttpMethods ) {
            if ( method.equalsIgnoreCase( httpServerExchange.getRequestMethod().toString() ) ) return;
        }
        throw new InvalidMethodException();
    }

    @Override
    @SuppressWarnings( "unchecked" )
    public void handleRequest( HttpServerExchange exchange ) throws Exception {
        if ( exchange.isInIoThread() ) {
            exchange.dispatch( this );
            return;
        }

        verifyHttpMethod( exchange );

        HandlersParameterArray parameterArray = new HandlersParameterArray( parameters.length );

        ServerExchange wrappedExchange = null;

        try {
            for ( int i = 0 ; i < parameters.length ; i++ ) {
                if ( this.handlersParametersMappers.containsKey( parameters[ i ] ) ) {
                    Object value = this.handlersParametersMappers.get( parameters[ i ] ).apply( exchange , parameterArray );
                    parameterArray.put( i , value );
                } else if ( this.httpParameterMappers.containsKey( parameters[ i ] ) ) {
                    Object value = this.httpParameterMappers.get( parameters[ i ] ).getKey().apply( exchange , parameters[ i ] );
                    if ( value == null ) {
                        if ( parameters[ i ].isAnnotationPresent( NotNull.class ) ) throw new MissingParameterException();
                        continue;
                    }
                    parameterArray.put( i , this.httpParameterMappers.get( parameters[ i ] ).getValue().apply( value ) );
                }
            }

            wrappedExchange = parameterArray.get( ServerExchange.class );

            if ( server.hasBeforeHandler() && wrappedExchange != null ) server.getBeforeHandler().accept( wrappedExchange );

            this.method.invoke( INSTANCE , parameterArray.getParameters() );

            if ( server.hasAfterHandler() && wrappedExchange != null ) server.getAfterHandler().accept( wrappedExchange , null );

        } catch ( Exception e ) {
            Pair< Integer, Object > exceptionInfo;

            Throwable t;

            if ( e instanceof InvocationTargetException ) {
                t = e.getCause();
            } else {
                t = e;
            }

            if ( this.server.getExceptionListener() != null ) {
                try {
                    this.server.getExceptionListener().accept( t );
                } catch ( Exception ignored ) {
                }
            }

            logger.debug( "Handler error, cause:" , t );

            if ( this.server.getExceptionToStatusCodeMapper().containsKey( t.getClass() ) ) {
                exceptionInfo = this.server.getExceptionToStatusCodeMapper().get( t.getClass() );
            } else if ( t instanceof IllegalArgumentException ) {
                exceptionInfo = Pair.of( 400 , t.getMessage() );
            } else {
                exceptionInfo = this.server.getExceptionToStatusCodeMapper().get( Exception.class );
            }

            Object mapper = Optional.ofNullable( exceptionInfo.getValue() ).orElse( t.getMessage() );
            String exceptionMessage = null;

            if ( mapper instanceof String ) {
                exceptionMessage = ( String ) mapper;
            } else if ( mapper instanceof Function ) {
                exceptionMessage = ( ( Function< Throwable, String > ) mapper ).apply( t );
            }

            exchange.setStatusCode( exceptionInfo.getKey() );
            exchange.getResponseHeaders().put( Headers.CONTENT_TYPE , "application/json" );
            exchange.getResponseSender().send( ServerExchange.writeValueAsString( new ApiResponse<>( t , exceptionMessage ) ) );
            exchange.endExchange();

            if ( server.hasAfterHandler() ) server.getAfterHandler().accept( wrappedExchange , t );
        }

        parameterArray.close();
    }
}
