package mx.com.gofarma.udis.enums;

public enum HttpMethod {
    GET, POST, PUT, HEAD, DELETE
}
