package mx.com.gofarma.udis.exceptions;

public class InvalidInjectedStateException extends RuntimeException {
    public InvalidInjectedStateException() {
    }

    public InvalidInjectedStateException( Exception e ) {
        super( "One of the injected object was rejected by the validation function" , e );
    }
}
