package mx.com.gofarma.udis.exceptions;

public class InvalidParameterTypeForFromFormDataAnnotationException extends RuntimeException {
    public InvalidParameterTypeForFromFormDataAnnotationException() {
    }

    public InvalidParameterTypeForFromFormDataAnnotationException( Exception e ) {
        super( "The FromFormData annotation is only accepted when the parameter is assignable to FormData" , e );
    }
}
