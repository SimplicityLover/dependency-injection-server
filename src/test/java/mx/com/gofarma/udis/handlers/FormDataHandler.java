package mx.com.gofarma.udis.handlers;

import mx.com.gofarma.udis.annotations.FromFormData;
import mx.com.gofarma.udis.annotations.Route;
import mx.com.gofarma.udis.enums.HttpMethod;
import mx.com.gofarma.udis.wrappers.ReadFile;
import mx.com.gofarma.udis.wrappers.ReadFormData;
import mx.com.gofarma.udis.wrappers.ServerExchange;

import java.util.List;

public class FormDataHandler {
    @Route( routes = "/", methods = HttpMethod.POST )
    public static void formData( ServerExchange exchange , @FromFormData ReadFormData body ) {
        List< ReadFile > files = body.getFilesOf( "file" );
        exchange.apiResponse( body );
    }
}
