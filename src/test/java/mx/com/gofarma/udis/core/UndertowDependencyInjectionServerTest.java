package mx.com.gofarma.udis.core;

import mx.com.gofarma.udis.handlers.FormDataHandler;

import static org.junit.jupiter.api.Assertions.*;

class UndertowDependencyInjectionServerTest {

    public static void main( String[] args ) {
        UndertowDependencyInjectionServer.builder().addHandlerClass( FormDataHandler.class ).run( 8080 , "0.0.0.0" );
    }
}